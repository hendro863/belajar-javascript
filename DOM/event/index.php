<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <style>
        .merah {
            background: red;
        }
        .black {
            border:2px solid black;
        }
    </style>
</head>
<body onload="alert('sudah selesai')">
    <h1>Belajar DOM javascript</h1>
    <img src="gbr/naruto.png" width="300" id="gambar" onclick="berubah()" class="black"><br>
    <input type="text" value="Coba" id="input">
    <script>
        var gbr = document.getElementById('gambar');
        gbr.width = 400;
        gbr.className += ' merah';
        var input = document.getElementById('input');

        input.value = "nama lengkap";
        input.style.border = '2px solid blue';
        input.style.backgroundColor = 'green';
    //------------------Event--------------------\\
         var a = 'ganti';
         function berubah(){
            if (a == 'ganti') {
                gbr.style.backgroundColor = 'yellow';
                a = 'gagal';
            }else {
                gbr.style.backgroundColor = 'red';
                a = 'ganti';
            }
        }
        // input.onclick = function (){
        //     berubah2();
        // };
        // function berubah2(){
        //     input.style.backgroundColor = 'yellow';
        // }
        // input.addEventListener('click',function(){
        //     input.style.backgroundColor = 'orange';
        // });
        input.addEventListener('mouseenter',function(){
            input.style.backgroundColor ='black';
        });
        input.addEventListener('mouseleave',function() {
            input.style.backgroundColor = 'purple';
        });
    </script>
</body>
</html>
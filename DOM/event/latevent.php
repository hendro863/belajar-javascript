<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <style>
        .merah {
            background: red;
        }
        .black {
            border:2px solid black;
        }
    </style>
</head>
<body onload="">
    <h1>Belajar DOM javascript</h1>
    <img src="gbr/naruto.png" width="300" id="gambar" onclick="berubah()" class="black"><br>
    <input type="text" value="Coba" id="input">
    Nama :<span id="p1"></span>
    <input type="submit" id="submit">
    <script>
        var gbr = document.getElementById('gambar');
        gbr.width = 400;
        gbr.className += ' merah';
        var input = document.getElementById('input');

        input.value = "nama lengkap";
        input.style.border = '2px solid blue';
        input.style.backgroundColor = 'green';
    //------------------Event--------------------\\
         var a = 'ganti';
         function berubah(){
            if (a == 'ganti') {
                gbr.style.backgroundColor = 'yellow';
                a = 'gagal';
            }else {
                gbr.style.backgroundColor = 'red';
                a = 'ganti';
            }
        }
        input.onclick = berubah();
        function berubah2(){
            input.style.backgroundColor = 'grey';
        }
        // input.addEventListener('click',function(){
        //     input.style.backgroundColor = 'orange';
        // });
        input.addEventListener('mouseenter',berubah2);
        input.addEventListener('mouseleave',function() {
            input.style.backgroundColor = 'purple';
        });

        var submit = document.getElementById('submit');
        var p1 = document.getElementById('p1');
        submit.onclick = function(){
            var text = input.value;
            p1.innerHTML = text;
        };
    </script>
</body>
</html>
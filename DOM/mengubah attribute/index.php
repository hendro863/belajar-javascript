<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <style>
        .merah {
            background: orange;
        }
    </style>
</head>
<body>
    <h1>Belajar DOM javascript</h1>
    <img src="gbr/naruto.png" width="300" id="gambar"><br>
    <input type="text" value="nama" id="input">
    <script>
        var gbr = document.getElementById('gambar');
        gbr.setAttribute('class','merah');
        var input = document.getElementById('input');
        gbr.width = 400;
        input.value = "nama lengkap";
    </script>
</body>
</html>
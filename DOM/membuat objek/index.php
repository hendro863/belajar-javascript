<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
    Nama : <span id="n"></span>
    Alamat : <span id="a"></span>
    Umur : <span id="u"></span>

    <script>
        // ------------ cara 1 ----------------
        // var bio = {
        //     nama : "hendro",
        //     alamat : "jalan tohjoyo",
        //     umur : 12
        // }
        // document.getElementById('nama').innerHTML = bio.nama;
        // document.getElementById('alamat').innerHTML = bio.alamat;
        // document.getElementById('umur').innerHTML = bio.umur;
        // ----------------||-----------------------
        //----------------- cara ke 2 --------------
        var developer = new Object();
        developer.nama = "hendro";
        developer.umur = 19;
        developer.alamat = 'jaalan tohjoyo no 5';

        document.getElementById('n').innerHTML = developer["nama"];
        document.getElementById('a').innerHTML = developer["alamat"];
        document.getElementById('u').innerHTML = developer["umur"];
    </script>
</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
    <div id="box"></div>
    <script>
        function pemainBola(name,umur){
            this.nama = name;
            this.umur = umur;
        }
        pemainBola.prototype.negara = 'Indonesia';
        var hendro = new pemainBola("Ronaldo",7);
        document.getElementById('box').innerHTML = hendro.negara+hendro.umur;
    </script>
</body>
</html>
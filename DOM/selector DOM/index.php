<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
    <h1>Belajar DOM Javascript</h1>
    <div id="box1"></div>
    <div class="boxes"></div>
    <div class="boxes"></div>
    <div class="boxes"></div>
    <ul>
        <li>HTML</li>
        <li>CSS</li>
        <li>Javascript</li>
    </ul>
    <script>
        document.getElementById('box1').innerHTML = "Hendro";
        document.getElementsByClassName('boxes')[0].innerHTML = "Senda";

        var tampil = document.getElementsByClassName('boxes');
        tampil[1].innerHTML = "Saputra";
        tampil[2].innerHTML = "Rahmat";

        document.getElementsByTagName('li')[0].innerHTML = "berubah 1";
        var tag = document.getElementsByTagName('li');
        tag[1].innerHTML = "berubah 2";
        tag[2].innerHTML = "berubah 3";
    </script>
</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <style>
        .merah {
            background: red;
        }
        .black {
            border:2px solid black;
        }
    </style>
</head>
<body>
    <h1>Belajar DOM javascript</h1>
    <img src="gbr/naruto.png" width="300" id="gambar" class="black"><br>
    <input type="text" value="Coba" id="input">
    <script>
        var gbr = document.getElementById('gambar');
        gbr.width = 400;
        gbr.className += ' merah';
        var input = document.getElementById('input');

        input.value = "nama lengkap";
        input.style.border = '2px solid blue';
        input.style.backgroundColor = 'green';
    </script>
</body>
</html>
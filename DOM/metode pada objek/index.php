<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
    <div id="box"></div>
    <script>
        var programmer = {
            nama : "Hendro Rahmat",
            umur : 12,
            ambilData : function(){
                return "Nama : "+this.nama+" Umur : "+this.umur;
            },
            siapkanData : function(nama){
                this.nama = nama;
                return nama;
            }
        }

        //document.getElementById('box').innerHTML = programmer.ambilData();
        programmer.siapkanData("Jevan");
        document.getElementById('box').innerHTML = programmer.ambilData();
    </script>
</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
    <div id="box">
        <div id="p1">Paragraf 1</div>
    </div>
    <script>
    //----------------Menambahkan Element-----------------\\
        var el = document.createElement('h2');
        var text = document.createTextNode('Belajar Programming Online');
        var box = document.getElementById('box');

        el.appendChild(text);
        box.appendChild(el);
    //----------------Menghapus Element----------------------\\
        //box.removeChild(el);
    //----------------Mengedit Element------------------------\\
        var el2 = document.createElement('h3');
        var text2 = document.createTextNode('Hello Javascript!!');
        el2.appendChild(text2);
        var p1 = document.getElementById('p1')
        //replaceChild(elementbaru,elementlama);
        box.replaceChild(el2,p1);
    </script>
</body>
</html>
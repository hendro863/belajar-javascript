<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Upload Progress Bar</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <main>
        <h1>Upload File</h1>
        <p>
            <input type="file" name="file" id="file-input">
            <label for="file-input">Select File</label>
        </p>
        <p>
            <progress id="upload-progress"></progress>
        </p>
        <p id="message"></p>
        <img src="" alt="" id="image">
    </main>
    <script src="progressbar.js"></script>
</body>
</html>
var file;

let fileInput, uploadProgress, message;

function init(){
    fileInput = document.getElementById('file-input');
    uploadProgress = document.getElementById('upload-progress');
    message = document.getElementById('message');

    fileInput.addEventListener('change', function () {
        let xhr = new XMLHttpRequest(),
            fd = new FormData();
        fd.append('file', fileInput.files[0]);
        console.log(fileInput.files);
        console.log(fileInput.files[0].name);

        xhr.upload.onloadstart = function (e) {
            uploadProgress.classList.add('visible');
            uploadProgress.value = 0;
            uploadProgress.max = e.total;
            message.textContent = 'Uploading...';
            fileInput.disabled = true;
        };

        xhr.upload.onprogress = function (e) {
            uploadProgress.value = e.loaded;
            console.log((e.loaded / e.total) * 100);
            uploadProgress.max = e.total;
        };

        xhr.upload.onloadend = function (e) {
            uploadProgress.classList.remove('visible');
            message.textContent = 'Complete!';
            fileInput.disabled = false;
        };

        xhr.onload = function () {
            var reader  = new FileReader();
            let typeFile = fileInput.files[0].type.split("/")[1];
            if (typeFile == 'jpeg' || typeFile == 'jpg' || typeFile == 'png') {
                reader.onload = function () {
                    document.getElementById("image").src = reader.result;
                }

                if (fileInput.files[0]) {
                    reader.readAsDataURL(fileInput.files[0]);
                }
            }
        }

        xhr.open('POST', 'catchfile.php', true);
        xhr.send(fd);
    })
}
init();
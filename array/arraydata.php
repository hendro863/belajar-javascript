<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <script>
        var hewan = ["gajah","macan","beruang"];
        //hewan.pop(); //menghapus elemen terakhir pada array;
        //hewan.shift(); //kebalikan dari pop
        //hewan.push("lele"); //menambahkan ke akhir elemen array
        //hewan.unshift("nyambek"); // kebalikan dari push
        hewan[hewan.length] = "banyak";

        hewan.toString();
        document.write(hewan);
    </script>
</head>
<body>

</body>
</html>
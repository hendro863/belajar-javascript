<!DOCTYPE html>
<html>
<head>
    <title>Belajar Jquery</title>
</head>
<body>
<!-- <button onclick="ubahTeks();">Tombol</button> -->
<button>Tombol</button>
<div id="ganti">
    Ini Teks
</div>
<script src="jquery.min.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" charset="utf-8">
    function ubahTeks(){
        document.getElementById('ganti').innerHTML = "teks baru";
    }

    $(document).ready(function() {
        $('button').click(function() {
            $('#ganti').html("Ini adalah jquery");
        });
    });
</script>
</body>
</html>
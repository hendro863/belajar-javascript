<div id="timer"></div>
<script>
    var detik = 5;
    var menit = 1;
    var jam = 0;

    function hitungMundur(){
        setTimeout(hitungMundur, 1000);
        //document.write("hello world");
        document.getElementById('timer').innerHTML = jam+" jam "+menit+ " menit "+detik+" detik ";
        detik-- ;
        if(detik == 0 && menit == 0 && jam == 0){
            document.write('berhenti');
        }

        if(detik < 0){
            detik = 3;
            menit --;
        }

        if(menit < 0){
            jam--;
            menit = 59;
        }
    }

    hitungMundur();
</script>
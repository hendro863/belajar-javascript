<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Belajar Javasccript Advance</title>
</head>
<body>
    <script>
        //-----Factory Pattern-----\\
            var peopleFactory = function (name, age, state){
                var temp = {};
                temp.age = age;
                temp.name = name;
                temp.state = state;

                temp.printPerson = function(){
                    console.log(this.name+", "+this.age+", "+this.state);
                };

                return temp;
            };

            var person1 = peopleFactory("hendro", 20, "Indonesia");
            var person2 = peopleFactory("senda", 22, "china");

            person1.printPerson();
            person2.printPerson();
        //-------------------------\\
    </script>
    <div id="belaajar-javascript">

    </div>
</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Prototype Pattern</title>
</head>
<body>
    <script>
        var peopleProto = function(){

        };

        peopleProto.prototype.name = 'no name';
        peopleProto.prototype.age = 'no age';
        peopleProto.prototype.city = 'no city';

        peopleProto.prototype.printPerson = function(){
            console.log(this.name+", "+this.age+", "+this.city);
        };

        var person1 = new peopleProto();
        person1.name = "hendro";
        person1.age = 20;
        person1.city = "indonesia";
        person1.printPerson();
        console.log('name' in person1); // mengambil property pada baris ke 13
        console.log(person1.hasOwnProperty('name')); // mengambil property pada baris ke 22 hasil di browser false jika property person1.name di comment
    </script>
</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>constructor pattern</title>
</head>
<body>
    <script>
        var peopleConstructor = function(name, age, state){
            this.name = name;
            this.age = age;
            this.state = state;

            this.printPerson = function (){
                console.log(this.name+", "+this.age+", "+this.state);
            };
        };

        var person1 = new peopleConstructor("hendrs", 20, "indonesia");
        person1.printPerson();
    </script>
</body>
</html>
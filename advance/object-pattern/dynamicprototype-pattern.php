<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Dynamic Prototype Pattern</title>
</head>
<body>
    <script>
        var peopleDynamicProto = function(name, age, state){
            this.name = name;
            this.age = age;
            this.state = state;

            if(typeof this.printPerson !== 'function'){
                peopleDynamicProto.prototype.printPerson = function(){
                    return this.name+", "+this.age+", "+this.state;
                };
            }
        };

        var person1 = new peopleDynamicProto("hendro",21,"Sidoarjo");
        document.write(person1.printPerson());
        console.log('name' in person1);
    </script>
</body>
</html>
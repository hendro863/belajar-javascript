<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Apply And Bind</title>
</head>
<body>
    <script>
        var obj = {num:2};

        var addToThis = function(a, b, c){
            return this.num + a + b + c;
        };
        var arr = [1,2,3];
        //console.log(addToThis.call(obj,2,3,4));
        //console.log(addToThis.apply(obj, arr));
        var bound = addToThis.bind(obj);
        console.dir(bound(1,2,3));
    </script>
</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Callback Function</title>
</head>
<body>
    <script>
        var add = function(a,b){
            return a + b;
        };

        var multiply = function(a,b){
            return a * b;
        };

        var doWhatEver = function(a,b){
            console.log("here are you two numbers back "+a+"  "+b);
        };

        var calc = function(num1, num2, callback){
            if (typeof callback == 'function') {
                return callback(num1,num2);
            }
            //return callback(num1, num2);
        };

        //console.log(calc(5,6,function(a,b){return a-b;}));
        console.log(calc(5,6,add));

        var myArr = [
            {
                num : 1,
                str : "banana"
            },
            {
                num : 2,
                str : "apple"
            }
        ];

        myArr.sort(function(val1, val2){
            if(val1.num < val2.num){
                return -1;
            }else {
                return 1;
            }
        });

        console.log(myArr);
    </script>
</body>
</html>
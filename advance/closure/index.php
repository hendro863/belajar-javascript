<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Javascript Closure</title>
</head>
<body>
    <script>

        var addTo = function(passed){
            var add = function(inner){
                return passed + inner;
            };

            return add;
        };
        var addThree = new addTo(2);
        console.dir(addThree(10));
    </script>
</body>
</html>